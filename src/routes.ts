import {Router} from 'express';
import { AcsController } from './controllers/AcsController'

const router = Router();

const acsController = new AcsController();

router.post("/agentes", acsController.create);
// Editar
// Delete
router.get("/agentes", acsController.show);
// Show one


export {router};