import { Column, CreateDateColumn, Entity, PrimaryColumn } from "typeorm";
import { v4 as uuid} from 'uuid';

@Entity("acs")
class Acs {
    @PrimaryColumn()
    readonly id: string;

    @Column()
    nome: string;

    @Column()
    email: string;

    @CreateDateColumn()
    inserido_em: Date

    constructor(){
        if (!this.id){
            this.id = uuid();
        }
    }
}

export {Acs}