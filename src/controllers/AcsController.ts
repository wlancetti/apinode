import {Request, Response} from 'express';
import { getCustomRepository } from 'typeorm';
import { Acs } from '../models/Acs';
import { AcsRepository } from '../repositories/AcsRepository'

class AcsController{
    async create (request: Request, response: Response){
        const {nome, email} = request.body;

        const acsRepository = getCustomRepository(AcsRepository);

        const acs = acsRepository.create({
            nome,
            email
        });

        await acsRepository.save(acs);

        return response.status(201).json(acs);
    }   

    async show (request: Request, response: Response){
        const acsRepository = getCustomRepository(AcsRepository);

        const all = await acsRepository.find();

        return response.json(all);
    }

    async edit (request: Request, response: Response){
        const {email, dados} = request.body;
        
        const acsRepository = getCustomRepository(AcsRepository);

        const AcsEdit = acsRepository.findOne(email);

        const acs = (await AcsEdit).nome = dados;

        acsRepository.save(acs);  
        
        return response.status(200).json(acs);
    }

    async delete (request: Request, response: Response){
        const {id} = request.body;

        const acsRepository = getCustomRepository(AcsRepository);

        acsRepository.delete(id);

        return response.status(200).json({mensagem: "ACS deletado com sucesso."});
    }

}

export { AcsController };