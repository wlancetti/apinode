import {MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateACS1618927025295 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: "ACS",
                columns: [
                    {
                        name: "id",
                        type: "uuid",
                        isPrimary: true
                    },
                    {
                        name: "nome",
                        type: "varchar"
                    },
                    {
                        name: "email",
                        type: "varchar"
                    },
                    {
                        name: "inserido_em",
                        type: "timestamp",
                        default: "now()"
                    }
               ] 

            }
            )
        
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("ACS");
    }

}
